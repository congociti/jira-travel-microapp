define (require)->

  config = require '../config'

  class ProjectModel extends Backbone.Model
    url: "#{config.restPath}/api/2/project/#{config.projectKey}"
    initialize: ->
      @fetch().done (d)->
        @id = d.id